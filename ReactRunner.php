<?php

namespace IPC\ReactPHP;

use Psr\Http\Message\ServerRequestInterface as Request;
use React\EventLoop\Factory;
use React\Http\Response;
use React\Http\Server;
use React\Promise\PromiseInterface;
use React\Socket\Server as Socket;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\HttpKernel\TerminableInterface;

class ReactRunner
{
    public function run(HttpKernelInterface $kernel, InputInterface $input, OutputInterface $output)
    {
        $exitCode    = 0;
        $loop        = Factory::create();

        if ($kernel instanceof KernelInterface) {
            $container = $kernel->getContainer();
            $container->set('react.loop', $loop);
        }

        $host   = $input->getOption(Constants::OPTION_HOST);
        $port   = $input->getOption(Constants::OPTION_PORT);
        $public = $input->getOption(Constants::OPTION_PUBLIC_DIR);
        $uri    = $host . ':' . $port;
        $socket = new Socket($uri, $loop);
        $server = new Server(function (Request $request) use ($kernel, $loop, $public, $output, &$exitCode) {

            $method  = $request->getMethod();
            $headers = $request->getHeaders();
            $content = $request->getBody();
            $files   = $request->getUploadedFiles();
            $uri     = $request->getUri();
            $query   = $uri->getQuery();
            $path    = $uri->getPath();
            $post    = [];
            $cookies = [];

            // serve file
            if (!empty($public) && is_dir($public)) {
                $file = $public . $path;
                if (is_file($file) && !preg_match('/\.php$/', $path, $matches)) {
                    $response = new Response(200, [], file_get_contents($file));
                    $this->writeOutput($output, $request, $response);

                    return $response;
                }
            }

            // parse post
            if (isset($headers['Content-Type']) &&
                (0 === strpos($headers['Content-Type'], 'application/x-www-form-urlencoded') &&
                    in_array(strtoupper($method), ['POST', 'PUT', 'DELETE', 'PATCH'], true))
            ) {
                parse_str($content, $post);
            }

            // parse cookies
            if (isset($headers['Cookie'])) {
                foreach ((array)$headers['Cookie'] as $cookieHeader) {
                    foreach (explode(';', $cookieHeader) as $cookie) {
                        list($name, $value) = explode('=', trim($cookie), 2);
                        $cookies[$name]     = urldecode($value);
                    }
                }
            }

            // create & handle request
            $symfonyRequest = SymfonyRequest::create($query, $method, $post, $cookies, $files, [], $content);
            $symfonyRequest->setMethod($method);
            $symfonyRequest->headers->replace($headers);
            $symfonyRequest->server->set('REQUEST_URI', $path);
            if (isset($headers['Host'])) {
                $symfonyRequest->server->set('SERVER_NAME', explode(':', $headers['Host'][0]));
            }
            $symfonyRequest->headers->replace($headers);
            $symfonyResponse = $kernel->handle($symfonyRequest);

            if ($kernel instanceof TerminableInterface) {
                $kernel->terminate($symfonyRequest, $symfonyResponse);
            }

            // handle & map response
            if ($symfonyResponse instanceof PromiseInterface) {
                $symfonyResponse->then(function (SymfonyResponse $symfonyResponse) use ($output, $request) {
                    $response = $this->response($symfonyResponse);
                    $this->writeOutput($output, $request, $response);

                    return $response;
                }, function ($error) use ($loop, $output, $request, &$exitCode) {
                    $exitCode = 255;
                    $response = $this->errorResponse();
                    $this->writeOutput($output, $request, $response);
                    $output->writeln('<fg=red>Exception: ' . (string) $error . '</>');
                    $loop->stop();

                    return $response;
                });
            } elseif ($symfonyResponse instanceof SymfonyResponse) {
                $response = $this->response($symfonyResponse);
                $this->writeOutput($output, $request, $response);

                return $response;
            } else {
                $output->writeln('<fg=red>Unsupported response type: ' . get_class($symfonyResponse) . '</>');
            }

            $exitCode = 255;
            $response = $this->errorResponse();
            $this->writeOutput($output, $request, $response);
            $loop->stop();

            return $response;
        });

        $server->listen($socket);

        $output->writeln('Listening on http://' . $host . ':' . $port);
        if (!empty($public) && is_dir($public)) {
            $output->writeln('Document root is ' . $public);
        }
        $output->writeln('Press Ctrl-C to quit.');

        $loop->run();

        return $exitCode;
    }

    /**
     * @param SymfonyResponse $response
     *
     * @return Response
     */
    private function response(SymfonyResponse $response)
    {
        return new Response(
            $response->getStatusCode(),
            $response->headers->all(),
            $response->getContent()
        );
    }

    /**
     * @return Response
     */
    private function errorResponse()
    {
        return new Response(
            500,
            ['Content-Type' => 'text/plain'],
            '500 Internal Server Error'
        );
    }

    /**
     * @param OutputInterface $output
     * @param Request         $request
     * @param Response        $response
     */
    private function writeOutput(OutputInterface $output, Request $request, Response $response)
    {
        $date   = date('D M ') .
            str_pad(date('j'), 2, ' ', STR_PAD_LEFT) .
            date(' H:i:s Y');
        $params = $request->getServerParams();
        $uri    = $params['REMOTE_ADDR'] . ':' . $params['REMOTE_PORT'];
        $path   = $request->getUri()->getPath();
        $status = $response->getStatusCode();

        $output->writeln(sprintf('[%s] <info>%s [%s]: %s</info>', $date, $uri, $status, $path));
    }
}
