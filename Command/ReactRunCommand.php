<?php

namespace IPC\ReactPHP\Command;

use IPC\ReactPHP\Constants;
use IPC\ReactPHP\ReactRunner;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class ReactRunCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('react:run')
            ->setDescription('Run ReactPHP instance')
            ->addOption(
                Constants::OPTION_HOST,
                null,
                InputOption::VALUE_REQUIRED,
                'Host to bind HTTP server to.',
                Constants::DEFAULT_HOST
            )
            ->addOption(
                Constants::OPTION_PORT,
                null,
                InputOption::VALUE_REQUIRED,
                'Port to bind HTTP server to.',
                Constants::DEFAULT_PORT
            )
            ->addOption(
                Constants::OPTION_KERNEL,
                null,
                InputOption::VALUE_REQUIRED,
                'FQCN of the Kernel which should be used.'
            )
            ->addOption(
                Constants::OPTION_PUBLIC_DIR,
                null,
                InputOption::VALUE_OPTIONAL,
                'Absolute path to the public directory.'
            );
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $kernelClass = $input->getOption(Constants::OPTION_KERNEL);

        /* @var $kernel KernelInterface */
        $kernel = new $kernelClass($environment = $input->getOption('env'), $environment !== 'prod');
        $kernel->boot();

        $runner = new ReactRunner();
        return $runner->run($kernel, $input, $output);
    }
}
