<?php

namespace IPC\ReactPHP;

interface Constants
{
    const OPTION_HOST        = 'host';
    const OPTION_PORT        = 'port';
    const OPTION_KERNEL      = 'kernel';
    const OPTION_PUBLIC_DIR  = 'public-dir';

    const DEFAULT_HOST      = '127.0.0.1';
    const DEFAULT_PORT      = '8000';
}
